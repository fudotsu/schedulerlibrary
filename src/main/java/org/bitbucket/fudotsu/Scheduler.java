package org.bitbucket.fudotsu;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

/*
 *   ================================================
 *   =  Mini class for work with scheduled tasks,
 *   =  Like JavaScript code style, and with milliseconds (not MC ticks).
 *   =  Written by Fudo Tsukiko
 *   ================================================
 */
public class Scheduler {

    private JavaPlugin plugin;

    public Scheduler(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public BukkitTask runTimeout(Runnable run, int inte) {
        int delay = (int) Math.ceil(inte / 50);
        return Bukkit.getScheduler().runTaskLater(plugin, run, delay);
    }
    public BukkitTask runTimeoutAsync(Runnable run, int inte) {
        int delay = (int) Math.ceil(inte / 50);
        return Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, run, delay);
    }
    public BukkitTask runAsync(Runnable run) {
        return Bukkit.getScheduler().runTaskAsynchronously(plugin, run);
    }
    public BukkitTask runInterval(Runnable run, int inte) {
        int delay = (int) Math.ceil(inte / 50);
        return Bukkit.getScheduler().runTaskTimer(plugin, run, delay, delay);
    }
    public BukkitTask runIntervalAsync(Runnable run, int inte) {
        int delay = (int) Math.ceil(inte / 50);
        return Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, run, delay, delay);
    }
    public void stopTask(int id) {
        Bukkit.getScheduler().cancelTask(id);
    }
    public void stopTasks(int... id) {
        for (int i = 0; i < id.length; i++)
            Bukkit.getScheduler().cancelTask(id[i]);
    }
    public boolean isRunned(int id) {
        return Bukkit.getScheduler().isCurrentlyRunning(id);
    }
    public void stopAll() {
        Bukkit.getScheduler().cancelTasks(plugin);
    }

}
